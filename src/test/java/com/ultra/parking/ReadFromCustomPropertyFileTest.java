//package com.ultra.parking;
//
//import com.ultra.parking.util.DisplayKey;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.core.env.Environment;
//
//@SpringBootTest
//public class ReadFromCustomPropertyFileTest {
//
//    @MockBean
//    DisplayKey displayKeyMock;
//
//    @MockBean
//    Environment environment;
//
//    String INVALID_VALUE = "Please check all Data";
//
//    @Test
//    void readByKeyTest() {
//        displayKeyMock.setEnvironment(environment);
//        String result = displayKeyMock.getValue("INVALID_VALUE");
//        result.equals(INVALID_VALUE);
//    }
//}
