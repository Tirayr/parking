package com.ultra.parking.model;

public enum Role {
    RESIDENT, ADMIN
}
