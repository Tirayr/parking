package com.ultra.parking.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommunityDto {

    private String address;

    private int countOfRooms;


    private String nameOfCommunity;

}
