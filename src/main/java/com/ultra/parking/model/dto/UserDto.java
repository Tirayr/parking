package com.ultra.parking.model.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    private String Name;

    private String surname;

    private String phoneNumber;

    private String emailAddress;

    private String communityAddress;
}
