package com.ultra.parking.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingDto {

    private boolean isFree;

    private boolean isUnderground;

    private int number;

    private String address;

    private int countOfSlots;

}
