package com.ultra.parking.model.dto;

import com.ultra.parking.model.SlotState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SlotDto {

    private int number;

    private int parkingNumber;

    private SlotState slotState;
}
