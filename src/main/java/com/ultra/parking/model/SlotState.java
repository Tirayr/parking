package com.ultra.parking.model;

public enum SlotState {

    BOOKED,
    BUSY,
    FREE

}