package com.ultra.parking.model.entity;

import com.ultra.parking.model.Role;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import org.springframework.lang.NonNull;

import java.util.List;


@Entity
public class User extends BaseEntity {

    @NonNull
    private String name;
    @NonNull
    private String surname;
    @NonNull
    @Column(unique = true)
    private String phoneNumber;
    @NonNull
    @Email(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    @Column(unique = true)
    private String emailAddress;
    @NonNull
    private String communityAddress;

    @Getter
    @Min(0)
    @Max(2)
    private int countOfBooks;
    @NonNull
    @Enumerated(EnumType.STRING)
    private Role role;

    @Getter
    @OneToOne(cascade = CascadeType.ALL)
    private Slot usedSlot;

    @Getter
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<Slot> bookedSlots;

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCommunityAddress() {
        return communityAddress;
    }

    public void setCommunityAddress(String communityAddress) {
        this.communityAddress = communityAddress;
    }

    @NonNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NonNull Role role) {
        this.role = role;
    }

    public void setUsedSlot(Slot usedSlot) {
        this.usedSlot = usedSlot;
    }

    public void setBookedSlots(List<Slot> bookedSlots) {
        this.bookedSlots = bookedSlots;
    }

    public void setCountOfBooks(int countOfBooks) {
        this.countOfBooks = countOfBooks;
    }
}
