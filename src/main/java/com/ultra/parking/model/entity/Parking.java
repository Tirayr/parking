package com.ultra.parking.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.lang.NonNull;

import java.util.List;

@Entity
public class Parking extends BaseEntity {

    @NonNull
    private boolean isFree;

    @NotNull
    private boolean isUnderground;

    @NotNull
    private int number;

    @NotNull
    private String address;


    //    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
//    @JoinTable(name = "parking_slot",
//            joinColumns = @JoinColumn(name = "parking_number", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "slot_parking_number", referencedColumnName = "id"))
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "parking_id")
    private List<Slot> slots;

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public boolean isUnderground() {
        return isUnderground;
    }

    public void setUnderground(boolean underground) {
        isUnderground = underground;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

//    public void setCommunity(Community community) {
//        this.community = community;
//    }
}
