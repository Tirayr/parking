package com.ultra.parking.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import org.springframework.lang.NonNull;

import java.util.List;

@Entity
public class Community extends BaseEntity {
    @Column(unique = true)
    @NonNull
    private String address;

    @NonNull
    @Min(1)
    @Max(100)
    private int countOfRooms;

    @Getter
    private int countOfSlots;

    @NonNull
    private String nameOfCommunity;

    //    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
//    @JoinTable(name = "community_parking",
//            joinColumns = @JoinColumn(name = "parking_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "parking_community_address", referencedColumnName = "id"))
//   @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "community")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "community_id")
    private List<Parking> parkingList;

    //    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "community_user",
//            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(name = "user_community_address", referencedColumnName = "id"))
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "community_id")
    private List<User> users;

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    public int getCountOfRooms() {
        return countOfRooms;
    }

    public void setCountOfRooms(int countOfRooms) {
        this.countOfRooms = countOfRooms;
    }

    @NonNull
    public String getNameOfCommunity() {
        return nameOfCommunity;
    }

    public void setNameOfCommunity(@NonNull String nameOfCommunity) {
        this.nameOfCommunity = nameOfCommunity;
    }

    public List<Parking> getParkingList() {
        return parkingList;
    }

    public void setParkingList(List<Parking> parkingList) {
        this.parkingList = parkingList;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void setCountOfSlots(int countOfSlots) {
        this.countOfSlots = countOfSlots;
    }
}
