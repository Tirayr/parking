package com.ultra.parking.repository;

import com.ultra.parking.model.entity.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {

    @Query(value = "SELECT * FROM ultra.slot WHERE slot.parking_number = :parkingNumber AND slot.number = :slotNumber",
            nativeQuery = true)
    Slot getSlotByParkingNumberAndNumber(@Param("parkingNumber") int parkingNumber, @Param("slotNumber") int slotNumber);

    @Query(value = "SELECT * FROM ultra.slot WHERE slot.parking_number = :parkingNumber",
            nativeQuery = true)
    List<Slot> getAllSlotByParkingNumber(@Param("parkingNumber") int parkingNumber);

    @Query(value = "SELECT * FROM ultra.slot WHERE slot.parking_number = :parkingNumber AND slot.slot_state = 'FREE'",
            nativeQuery = true)
    List<Slot> getAllFreeSlotByParkingNumber(@Param("parkingNumber") int parkingNumber);

    @Query(value = "SELECT * FROM ultra.slot WHERE slot.parking_number = :number AND slot.slot_state = 'BOOKED'",
            nativeQuery = true)
    List<Slot> getAllBookedSlotsByParkingNumber(@Param("number") int number);

    @Query(value = "SELECT * FROM ultra.slot WHERE slot.slot_state = 'BOOKED' AND slot.booking_time < CURRENT_TIME",
            nativeQuery = true)
    List<Slot> getAllExpiredBookedSlots();

    @Query(value = "SELECT user_id FROM ultra.slot WHERE slot.parking_number = :parkingNumber AND slot.number = :slotNumber",
            nativeQuery = true)
    String getUserFromSlot(@Param("parkingNumber") int parkingNumber, @Param("slotNumber") int slotNumber);

    @Query(value = "UPDATE ultra.slot SET " +
            "slot.slot_state = 'FREE', slot.booking_time = null WHERE (id = :id)",
            nativeQuery = true)
    void rollbackSlot(@Param("id") Long id);
}