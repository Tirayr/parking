package com.ultra.parking.repository;

import com.ultra.parking.model.entity.Community;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface CommunityRepository extends JpaRepository<Community, Long> {

    @Query(value = "SELECT * FROM ultra.community WHERE community.address = :address",
            nativeQuery = true)
    Community findByAddress(@Param("address") String address);
}
