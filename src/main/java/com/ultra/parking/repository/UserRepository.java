package com.ultra.parking.repository;

import com.ultra.parking.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM ultra.user WHERE user.email_address = :email",
            nativeQuery = true)
    User findByEmail(@Param("email") String email);

    @Query(value = "SELECT * FROM ultra.user WHERE user.id = :id",
            nativeQuery = true)
    User getUserById(@Param("id") long id);
}
