package com.ultra.parking.repository;

import com.ultra.parking.model.entity.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ParkingRepository extends JpaRepository<Parking, Long> {
    @Query(value = "SELECT * FROM ultra.parking WHERE parking.address = :address",
            nativeQuery = true)
    List<Parking> getAllByAddress(@Param("address") String address);

    @Query(value = "SELECT * FROM ultra.parking WHERE parking.address = :address AND parking.number = :number",
            nativeQuery = true)
    Parking getParkingByAddressAndNumber(@Param("address") String address, @Param("number") int number);
}