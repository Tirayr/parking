package com.ultra.parking.service;

import com.ultra.parking.model.entity.Slot;

import java.util.List;

public interface SlotService {
    void add(List<Slot> slots, String address);

    Slot getSlotByAddressAndParkingNumber(String address, int parkingNumber, int slotNumber);

    List<Slot> getAllSlotsByParkingNumber(String address, int parkingNumber);

    List<Slot> getAllSlotsByAddress(String address);

    List<Slot> getAllFreeSlotsByAddressAndParkingNumber(String address, int parkingNumber);

    List<Slot> getAllFreeSlotsByAddress(String address);

    List<Slot> getAllBooksSlotsByAddress(String address);
}
