package com.ultra.parking.service;

import com.ultra.parking.model.entity.Parking;

import java.util.List;

public interface ParkingService {
    void add(List<Parking> parkings);

    List<Parking> getAllParkingsByAddress(String address);

    Parking getParkingByAddressAndNumber(String address, int number);
}
