package com.ultra.parking.service;

import com.ultra.parking.model.entity.Slot;
import com.ultra.parking.model.entity.User;

import java.util.List;

public interface UserService {
    void add(List<User> users);

    Slot bookSlotByAddressAndNumber(String email, String address, int parkingNumber, int slotNumber);

    User getUserByEmail(String email);

    Slot takeSlot(String email, String address, int parkingNumber, int slotNumber);

    Slot releaseSlot(String email, String address, int parkingNumber, int slotNumber);
}
