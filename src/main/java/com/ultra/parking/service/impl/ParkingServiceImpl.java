package com.ultra.parking.service.impl;

import com.ultra.parking.model.SlotState;
import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.repository.CommunityRepository;
import com.ultra.parking.repository.ParkingRepository;
import com.ultra.parking.service.ParkingService;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.CommunityNotFoundException;
import com.ultra.parking.util.exception.InvalidValuePassedException;
import com.ultra.parking.util.validator.ParkingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private ParkingRepository repository;
    @Autowired
    private CommunityRepository communityRepository;

    @Autowired
    private DisplayKey displayKey;
    @Autowired
    private ParkingValidator parkingValidator;

    /*
     *  Do some validations, and commit data to DB
     *
     * parking address and community address should be same
     * count of slots should be less dan 70% of rooms count
     * */
    @Override
    public void add(List<Parking> parkings) {
        parkings.forEach(parking -> {
            if (parking.getAddress() == null)
                throw new InvalidValuePassedException(displayKey.getValue("INVALID_VALUE" + " :" + "Parkin address is mandatory"));

            Community community = communityRepository.findByAddress(parking.getAddress());
            if (community != null) {
                if (parkingValidator.isValidParkingData(parking, community)) {
                    parking.getSlots().forEach(slot -> {
                        slot.setSlotState(SlotState.FREE);
                        slot.setParkingNumber(parking.getNumber());
                    });
                    community.getParkingList().add(parking);
                    repository.save(parking);
                }
            } else throw new CommunityNotFoundException(displayKey.getValue("COMMUNITY_NOT_FOUND"));
        });
    }

    @Override
    public List<Parking> getAllParkingsByAddress(String address) {
        return repository.getAllByAddress(address);
    }

    @Override
    public Parking getParkingByAddressAndNumber(String address, int number) {
        return repository.getParkingByAddressAndNumber(address, number);
    }
}
