package com.ultra.parking.service.impl;

import com.ultra.parking.model.SlotState;
import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.model.entity.Slot;
import com.ultra.parking.model.entity.User;
import com.ultra.parking.repository.SlotRepository;
import com.ultra.parking.repository.UserRepository;
import com.ultra.parking.service.CommunityService;
import com.ultra.parking.service.ParkingService;
import com.ultra.parking.service.SlotService;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.CommunityNotFoundException;
import com.ultra.parking.util.exception.ParkingNotFountException;
import com.ultra.parking.util.validator.SlotValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class SlotServiceImpl implements SlotService {

    @Autowired
    private SlotRepository repository;
    @Autowired
    private ParkingService parkingService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DisplayKey displayKey;
    @Autowired
    private SlotValidator slotValidator;

    @Override
    public void add(List<Slot> slots, String address) {
        slots.forEach(slot -> {
            Parking parking = parkingService.getParkingByAddressAndNumber(address, slot.getParkingNumber());
            if (parking != null) {
                Community community = communityService.getCommunityByAddress(address);
                if (slotValidator.isValidSlotData(1, community)) {
                    slot.setSlotState(SlotState.FREE);
                    slot.setNumber(parking.getSlots().size() + 1);
                    parking.getSlots().add(slot);
                    repository.save(slot);
                }
            } else throw new ParkingNotFountException(displayKey.getValue("PARKING_NOT_FOUNT"));
        });
    }

    @Override
    public Slot getSlotByAddressAndParkingNumber(String address, int parkingNumber, int slotNumber) {
        Parking parking = parkingService.getParkingByAddressAndNumber(address, parkingNumber);
        if (parking != null) {
            Slot slot = repository.getSlotByParkingNumberAndNumber(parkingNumber, slotNumber);
            if (slot == null)
                throw new ParkingNotFountException(displayKey.getValue("SLOT_NOT_FOUNT"));
            return slot;
        } else throw new ParkingNotFountException(displayKey.getValue("PARKING_NOT_FOUNT"));
    }

    @Override
    public List<Slot> getAllSlotsByParkingNumber(String address, int parkingNumber) {
        Parking parking = parkingService.getParkingByAddressAndNumber(address, parkingNumber);
        if (parking != null) {
            List<Slot> slotList = repository.getAllSlotByParkingNumber(parkingNumber);
            if (slotList == null)
                throw new ParkingNotFountException(displayKey.getValue("SLOT_NOT_FOUNT"));
            return slotList;
        } else throw new ParkingNotFountException(displayKey.getValue("PARKING_NOT_FOUNT"));
    }

    @Override
    public List<Slot> getAllSlotsByAddress(String address) {
        Community community = communityService.getCommunityByAddress(address);
        if (community != null) {
            ArrayList<Slot> slotList = new ArrayList<>();
            community.getParkingList().forEach(parking -> {
                getAllSlotsByParkingNumber(address, parking.getNumber()).forEach(slot -> slotList.add(slot));
            });
            return slotList;
        } else throw new CommunityNotFoundException(displayKey.getValue("COMMUNITY_NOT_FOUND"));
    }

    @Override
    public List<Slot> getAllFreeSlotsByAddressAndParkingNumber(String address, int parkingNumber) {
        Parking parking = parkingService.getParkingByAddressAndNumber(address, parkingNumber);
        if (parking != null) {
            List<Slot> slotList = repository.getAllFreeSlotByParkingNumber(parkingNumber);
            if (slotList == null)
                throw new ParkingNotFountException(displayKey.getValue("SLOT_NOT_FOUNT"));
            return slotList;
        } else throw new ParkingNotFountException(displayKey.getValue("PARKING_NOT_FOUNT"));
    }

    @Override
    public List<Slot> getAllFreeSlotsByAddress(String address) {
        Community community = communityService.getCommunityByAddress(address);
        if (community != null) {
            ArrayList<Slot> slotList = new ArrayList<>();
            community.getParkingList().forEach(parking -> {
                getAllFreeSlotsByAddressAndParkingNumber(address, parking.getNumber()).forEach(slot -> slotList.add(slot));
            });
            return slotList;
        } else throw new CommunityNotFoundException(displayKey.getValue("COMMUNITY_NOT_FOUND"));
    }

    @Override
    public List<Slot> getAllBooksSlotsByAddress(String address) {
        Community community = communityService.getCommunityByAddress(address);
        if (community != null) {
            ArrayList<Slot> slotList = new ArrayList<>();
            community.getParkingList().forEach(parking -> {
                repository.getAllBookedSlotsByParkingNumber(parking.getNumber()).forEach(slot -> slotList.add(slot));
            });
            return slotList;
        } else throw new CommunityNotFoundException(displayKey.getValue("COMMUNITY_NOT_FOUND"));
    }

    @Scheduled(timeUnit = TimeUnit.MINUTES, fixedDelay = 5)
    public void releaseSlot() {
        List<Slot> bookedSlotList = repository.getAllExpiredBookedSlots();
        if (!bookedSlotList.isEmpty()) {
            bookedSlotList.forEach(slot -> {
                String user_id = repository.getUserFromSlot(slot.getParkingNumber(), slot.getNumber());
                User user = userRepository.getUserById(Long.parseLong(user_id));
                int countOfBooks = user.getCountOfBooks();
                user.setCountOfBooks(countOfBooks - 1);
                user.getBookedSlots().remove(slot);
                userRepository.save(user);

                slot.setSlotState(SlotState.FREE);
                slot.setBookingTime(null);
                repository.save(slot);
            });
        }
    }
}