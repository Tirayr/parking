package com.ultra.parking.service.impl;

import com.ultra.parking.model.Role;
import com.ultra.parking.model.SlotState;
import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.repository.CommunityRepository;
import com.ultra.parking.service.CommunityService;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.InvalidValuePassedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComunnityServiceImpl implements CommunityService {

    @Autowired
    private CommunityRepository repository;

    @Autowired
    private DisplayKey displayKey;

    /*
     *  Do some validations, and commit data to DB
     *
     * parking address and community address should be same
     * count of slots should be less dan 70% of rooms count
     * */
    @Override
    public void add(Community community) {
        String communityAddress = community.getAddress();
        int countOfSlotsInParking = 0;
        if (!community.getParkingList().isEmpty()) {
            for (Parking parking : community.getParkingList()) {
                countOfSlotsInParking += parking.getSlots().size();
                parking.setAddress(communityAddress);
                parking.getSlots().forEach(slot -> {
                    slot.setSlotState(SlotState.FREE);
                    slot.setParkingNumber(parking.getNumber());
                });
            }

        }
        community.setCountOfSlots(countOfSlotsInParking);
        int allowedCountOfslots = community.getCountOfRooms() * 3 * 70 / 100;
        if (countOfSlotsInParking > allowedCountOfslots)
            throw new InvalidValuePassedException(displayKey.getValue("INVALID_VALUE"));

        if (!community.getUsers().isEmpty()) {
            community.getUsers().forEach(user -> {
                user.setRole(Role.RESIDENT);
                user.setCommunityAddress(communityAddress);
            });
        }
        repository.save(community);
    }

    @Override
    public Community getCommunityByAddress(String address) {
        return repository.findByAddress(address);
    }
}
