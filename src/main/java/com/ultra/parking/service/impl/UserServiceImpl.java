package com.ultra.parking.service.impl;

import com.ultra.parking.model.Role;
import com.ultra.parking.model.SlotState;
import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Slot;
import com.ultra.parking.model.entity.User;
import com.ultra.parking.repository.CommunityRepository;
import com.ultra.parking.repository.SlotRepository;
import com.ultra.parking.repository.UserRepository;
import com.ultra.parking.service.SlotService;
import com.ultra.parking.service.UserService;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.CommunityNotFoundException;
import com.ultra.parking.util.exception.InvalidValuePassedException;
import com.ultra.parking.util.exception.UnacceptableActionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private CommunityRepository communityRepository;

    @Autowired
    private SlotService slotService;

    @Autowired
    private SlotRepository slotRepository;

    @Autowired
    private DisplayKey displayKey;

    @Override
    public void add(List<User> users) {
        users.forEach(user -> {
            if (user.getCommunityAddress() == null)
                throw new InvalidValuePassedException(displayKey.getValue("INVALID_VALUE" + " :" + "Community address is mandatory"));
            Community community = communityRepository.findByAddress(user.getCommunityAddress());
            if (community != null) {
                user.setRole(Role.RESIDENT);
                community.getUsers().add(user);
                repository.save(user);
            } else throw new CommunityNotFoundException(displayKey.getValue("COMMUNITY_NOT_FOUND"));
        });
    }

    @Override
    public User getUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public Slot bookSlotByAddressAndNumber(String email, String address, int parkingNumber, int slotNumber) {
        Slot slot = slotService.getSlotByAddressAndParkingNumber(address, parkingNumber, slotNumber);
        if (slot.getSlotState() == SlotState.FREE) {
            User user = getUserByEmail(email);
            int countofBooks = user.getCountOfBooks();
            user.setCountOfBooks(countofBooks + 1);
            user.getBookedSlots().add(slot);
            repository.save(user);
            slot.setSlotState(SlotState.BOOKED);
            slot.setBookingTime(LocalDateTime.now().plusHours(1));
            slotRepository.save(slot);
            return slot;
        }
        throw new UnacceptableActionException(displayKey.getValue("UNACCEPTABLE_ACTION"));
    }

    @Override
    public Slot takeSlot(String email, String address, int parkingNumber, int slotNumber) {
        User user = getUserByEmail(email);
        Slot slot = slotService.getSlotByAddressAndParkingNumber(address, parkingNumber, slotNumber);
        if (!user.getBookedSlots().isEmpty() && !user.getBookedSlots().contains(slot))
            throw new UnacceptableActionException(displayKey.getValue("UNACCEPTABLE_ACTION") + " :" + "You have book slot, please take it");

        if (slot.getSlotState() == SlotState.BOOKED) {
            if (!user.getBookedSlots().contains(slot))
                throw new UnacceptableActionException(displayKey.getValue("UNACCEPTABLE_ACTION") + " :"
                        + "You can not take this slot, because this booked by other user");
            else {
                int countOfBooks = user.getCountOfBooks();
                user.getBookedSlots().remove(slot);
                user.setUsedSlot(slot);
                user.setCountOfBooks(countOfBooks - 1);
                repository.save(user);

                slot.setSlotState(SlotState.BUSY);
                slot.setBookingTime(null);
                slotRepository.save(slot);
            }
        } else if (slot.getSlotState() == SlotState.FREE) {
            user.setUsedSlot(slot);
            repository.save(user);
            slot.setSlotState(SlotState.BUSY);
            slotRepository.save(slot);
        }
        return slot;
    }

    @Override
    public Slot releaseSlot(String email, String address, int parkingNumber, int slotNumber) {
        User user = getUserByEmail(email);
        Slot slot = user.getUsedSlot();
        user.setUsedSlot(null);
        repository.save(user);
        slot.setSlotState(SlotState.FREE);
        slotRepository.save(slot);
        return slot;
    }


}