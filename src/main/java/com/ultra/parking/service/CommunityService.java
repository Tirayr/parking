package com.ultra.parking.service;

import com.ultra.parking.model.entity.Community;

public interface CommunityService {

    void add(Community community);

    public Community getCommunityByAddress(String address);
}
