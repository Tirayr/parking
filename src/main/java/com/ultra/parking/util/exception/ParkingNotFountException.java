package com.ultra.parking.util.exception;

public class ParkingNotFountException extends RuntimeException {
    public ParkingNotFountException(String message) {
        super(message);
    }
}
