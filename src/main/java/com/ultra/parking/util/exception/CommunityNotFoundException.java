package com.ultra.parking.util.exception;

public class CommunityNotFoundException extends RuntimeException {

    public CommunityNotFoundException(String message) {
        super(message);
    }
}
