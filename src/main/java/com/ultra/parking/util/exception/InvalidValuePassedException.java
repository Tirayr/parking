package com.ultra.parking.util.exception;

public class InvalidValuePassedException extends RuntimeException {
    public InvalidValuePassedException(String message) {
        super(message);
    }
}
