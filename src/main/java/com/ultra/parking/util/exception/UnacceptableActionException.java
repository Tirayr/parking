package com.ultra.parking.util.exception;

public class UnacceptableActionException extends RuntimeException {
    public UnacceptableActionException(String message) {
        super(message);
    }
}
