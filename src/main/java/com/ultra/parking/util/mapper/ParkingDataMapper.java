package com.ultra.parking.util.mapper;

import com.ultra.parking.model.dto.ParkingDto;
import com.ultra.parking.model.entity.Parking;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParkingDataMapper {

    public List<ParkingDto> mapParkingListToDto(List<Parking> parkings) {
        ArrayList parkingDtoList = new ArrayList<ParkingDto>();
        parkings.forEach(parking -> {
            ParkingDto dto = mapParkingToDto(parking);
            parkingDtoList.add(dto);
        });
        return parkingDtoList;
    }

    public ParkingDto mapParkingToDto(Parking parking) {
        ParkingDto dto = new ParkingDto();
        dto.setFree(parking.isFree());
        dto.setUnderground(parking.isUnderground());
        dto.setNumber(parking.getNumber());
        dto.setAddress(parking.getAddress());
        dto.setCountOfSlots(parking.getSlots().size());

        return dto;
    }
}
