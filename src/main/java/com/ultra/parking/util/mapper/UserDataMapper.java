package com.ultra.parking.util.mapper;

import com.ultra.parking.model.dto.UserDto;
import com.ultra.parking.model.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDataMapper {

    public List<UserDto> mapUserListToDto(List<User> users) {
        ArrayList userDtoList = new ArrayList<UserDto>();
        users.forEach(user -> {
            UserDto dto = mapUserToDto(user);
            userDtoList.add(dto);
        });
        return userDtoList;
    }

    public UserDto mapUserToDto(User user) {
        UserDto dto = new UserDto();
        dto.setName(user.getName());
        dto.setSurname(user.getSurname());
        dto.setPhoneNumber(user.getPhoneNumber());
        dto.setEmailAddress(user.getEmailAddress());
        dto.setCommunityAddress(user.getCommunityAddress());

        return dto;
    }
}
