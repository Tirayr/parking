package com.ultra.parking.util.mapper;

import com.ultra.parking.model.dto.SlotDto;
import com.ultra.parking.model.entity.Slot;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SlotDataMapper {
    public List<SlotDto> mapSlotToDto(List<Slot> slots) {
        ArrayList slotDtoList = new ArrayList<SlotDto>();
        slots.forEach(slot -> {
            SlotDto dto = new SlotDto();
            dto.setNumber(slot.getNumber());
            dto.setSlotState(slot.getSlotState());
            dto.setParkingNumber(slot.getParkingNumber());
            slotDtoList.add(dto);
        });
        return slotDtoList;
    }
}
