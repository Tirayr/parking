package com.ultra.parking.util.mapper;

import com.ultra.parking.model.dto.CommunityDto;
import com.ultra.parking.model.entity.Community;
import org.springframework.stereotype.Component;

@Component
public class CommunityDataMapper {
    public CommunityDto mapCommunityToDto(Community community) {
        CommunityDto dto = new CommunityDto();
        dto.setAddress(community.getAddress());
        dto.setCountOfRooms(community.getCountOfRooms());
        dto.setNameOfCommunity(community.getNameOfCommunity());
        return dto;
    }

}
