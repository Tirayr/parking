package com.ultra.parking.util.validator;

import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.DublicatDataException;
import com.ultra.parking.util.exception.InvalidValuePassedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParkingValidator {

    @Autowired
    private DisplayKey displayKey;

    @Autowired
    private SlotValidator slotValidator;

    public boolean isValidParkingData(Parking parking, Community community) {
        for (int i = 0; i < community.getParkingList().size(); i++) {
            Parking park = community.getParkingList().get(i);
            if (parking.getNumber() == park.getNumber())
                throw new DublicatDataException(displayKey.getValue("DUBLICATE_PARKING_NUMBNER"));
        }
        return slotValidator.isValidSlotData(parking.getSlots().size(), community);
    }

}
