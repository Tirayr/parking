package com.ultra.parking.util.validator;

import com.ultra.parking.model.entity.Community;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.util.DisplayKey;
import com.ultra.parking.util.exception.DublicatDataException;
import com.ultra.parking.util.exception.InvalidValuePassedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SlotValidator {

    @Autowired
    private DisplayKey displayKey;

    public boolean isValidSlotData(int countOfGivenSlots, Community community) {
        int allowedCountOfslots = community.getCountOfRooms() * 3 * 70 / 100;
        int currentCountOfSlots = community.getCountOfSlots() + countOfGivenSlots;
        if (currentCountOfSlots > allowedCountOfslots)
            throw new InvalidValuePassedException(displayKey.getValue("INVALID_VALUE") + " :" + displayKey.getValue("INVALID_VALUE_SLOTS"));
        community.setCountOfSlots(currentCountOfSlots);
        return true;
    }
}
