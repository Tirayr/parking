package com.ultra.parking.controller;

import com.ultra.parking.model.dto.CommunityDto;
import com.ultra.parking.model.entity.Community;
import com.ultra.parking.service.impl.ComunnityServiceImpl;
import com.ultra.parking.util.mapper.CommunityDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/community")
public class CommunityController {

    @Autowired
    private ComunnityServiceImpl service;

    @Autowired
    CommunityDataMapper mapper;

    /*
     * Handle request by url /community/create
     * Methode create new community
     *
     * @RequestBody Community community
     * */
    @PostMapping(value = "/create")
    @Transactional
    public ResponseEntity<CommunityDto> addNewCommunity(@RequestBody Community community) {
        CommunityDto dto = mapper.mapCommunityToDto(community);
        service.add(community);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    /*
     * Handle request by url /community/getByAddress
     * Methode get community by address
     *
     * @RequestParam String address
     * */
    @GetMapping(value = "/getByAddress")
    public ResponseEntity<Community> getCommunityByAddress(@RequestParam String address) {
        Community community = service.getCommunityByAddress(address);
        return new ResponseEntity(community, HttpStatus.OK);
    }

}
