package com.ultra.parking.controller;

import com.ultra.parking.model.dto.ParkingDto;
import com.ultra.parking.model.entity.Parking;
import com.ultra.parking.service.impl.ParkingServiceImpl;
import com.ultra.parking.util.mapper.ParkingDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/parking")
public class ParkingController {

    @Autowired
    private ParkingServiceImpl service;

    @Autowired
    private ParkingDataMapper mapper;

    /*
     * Handle request by url /parking/add
     * Methode add new parking to community
     *
     * @RequestBody List<Parking> parkings
     * */
    @PostMapping(value = "/add")
    @Transactional
    public ResponseEntity<List<ParkingDto>> addNewParking(@RequestBody List<Parking> parkings) {
        List<ParkingDto> dtoList = mapper.mapParkingListToDto(parkings);
        service.add(parkings);
        return new ResponseEntity(dtoList, HttpStatus.OK);
    }

    /*
     * Handle request by url /parking/getByAddress
     * Methode get parking by address
     *
     * @RequestParam String address
     * */
    @GetMapping(value = "/getByAddress")
    public ResponseEntity<List<ParkingDto>> getByAddress(@RequestParam String address) {
        List<Parking> parkingList = service.getAllParkingsByAddress(address);
        List<ParkingDto> dtoList = mapper.mapParkingListToDto(parkingList);
        return new ResponseEntity(dtoList, HttpStatus.OK);
    }

    /*
     * Handle request by url /parking/getByAddress
     * Methode get parking by address
     * */
    @GetMapping(value = "/getByAddressAndNumber")
    public ResponseEntity<ParkingDto> getByAddressAndNumber(@RequestParam String address, @RequestParam int number) {
        Parking parking = service.getParkingByAddressAndNumber(address, number);
        ParkingDto dto = mapper.mapParkingToDto(parking);
        return new ResponseEntity(dto, HttpStatus.OK);
    }
}