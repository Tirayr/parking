package com.ultra.parking.controller;

import com.ultra.parking.model.dto.SlotDto;
import com.ultra.parking.model.entity.Slot;
import com.ultra.parking.service.impl.SlotServiceImpl;
import com.ultra.parking.util.mapper.SlotDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/slot")
public class SlotController {

    @Autowired
    private SlotServiceImpl service;


    @Autowired
    SlotDataMapper mapper;

    /*
     * Handle request by url /slot/add
     * Methode create and add new slot to parking community
     *
     * @RequestParam String address, for get parking
     * @RequestBody Slot slot
     * */
    @PostMapping(value = "/create")
    @Transactional
    public ResponseEntity<List<SlotDto>> createAndAddSlot(@RequestParam String address, @RequestBody List<Slot> slots) {
        service.add(slots, address);
        List<SlotDto> dtoList = mapper.mapSlotToDto(slots);
        return new ResponseEntity(dtoList, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getAllSlotsByAddress
     * Methode get all slots by addres
     *
     * @Parametrs
     * @RequestParam String address
     * */
    @GetMapping(value = "/getAllSlotsByAddress")
    public ResponseEntity<List<Slot>> getAllSlotsByAddress(@RequestParam String address) {
        List<Slot> slotList = service.getAllSlotsByAddress(address);
        return new ResponseEntity(slotList, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getAllSlotsByParkingNumber
     * Methode get all slots by addres and parking number
     *
     * @Parametrs
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * */
    @GetMapping(value = "/getAllSlotsByParkingNumber")
    public ResponseEntity<List<Slot>> getAllSlotsByParkingNumber(@RequestParam String address,
                                                                 @RequestParam int parkingNumber) {
        List<Slot> slotList = service.getAllSlotsByParkingNumber(address, parkingNumber);
        return new ResponseEntity(slotList, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getByNumber
     * Methode get slot by address, parking number and slot number
     *
     * @Parametrs
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * @RequestParam int slotNumbergetByNumber
     * */
    @GetMapping(value = "/getByNumber")
    public ResponseEntity<Slot> getSlotByNumber(@RequestParam String address,
                                                @RequestParam int parkingNumber,
                                                @RequestParam int slotNumber) {
        Slot slot = service.getSlotByAddressAndParkingNumber(address, parkingNumber, slotNumber);
        return new ResponseEntity(slot, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getAllFreeSlotsByAddressAndParkingNumber
     * Methode get all free slots by addres and parking number
     *
     * @Parametrs
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * */
    @GetMapping(value = "/getAllFreeSlotsByAddressAndParkingNumber")
    public ResponseEntity<List<Slot>> getAllFreeSlotsByAddressAndParkingNumber(@RequestParam String address, @RequestParam int parkingNumber) {
        List<Slot> slotList = service.getAllFreeSlotsByAddressAndParkingNumber(address, parkingNumber);
        return new ResponseEntity(slotList, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getAllFreeSlotsByAddress
     * Methode get all free slots by addres(for community)
     *
     * @Parametrs
     * @RequestParam String address
     * */
    @GetMapping(value = "/getAllFreeSlotsByAddress")
    public ResponseEntity<List<Slot>> getAllFreeSlotsByAddress(@RequestParam String address) {
        List<Slot> slotList = service.getAllFreeSlotsByAddress(address);
        return new ResponseEntity(slotList, HttpStatus.OK);
    }

    /*
     * Handle request by url /slot/getAllBooksSlotsByAddress
     * Methode get all booked slots by addres(for community)
     *
     * @Parametrs
     * @RequestParam String address
     * */
    @GetMapping(value = "/getAllBooksSlotsByAddress")
    public ResponseEntity<List<Slot>> getAllBooksSlotsByAddress(@RequestParam String address) {
        List<Slot> slotList = service.getAllBooksSlotsByAddress(address);
        return new ResponseEntity(slotList, HttpStatus.OK);
    }
}
