package com.ultra.parking.controller;


import com.ultra.parking.model.dto.UserDto;
import com.ultra.parking.model.entity.Slot;
import com.ultra.parking.model.entity.User;
import com.ultra.parking.service.impl.UserServiceImpl;
import com.ultra.parking.util.mapper.UserDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserServiceImpl service;

    @Autowired
    private UserDataMapper mapper;

    /*
     * Handle request by url /user/add
     * Methode add new user to community
     *
     * @RequestBody List<User> users
     * */
    @PostMapping(value = "/add")
    @Transactional
    public ResponseEntity<List<UserDto>> addNewUser(@RequestBody List<User> users) {
        List<UserDto> dtoList = mapper.mapUserListToDto(users);
        service.add(users);
        return new ResponseEntity(dtoList, HttpStatus.OK);
    }

    /*
     * Handle request by url /user/getByEmail
     * Methode get user by email
     *
     * @RequestParam String email
     * */
    @GetMapping(value = "/getByEmail")
    public ResponseEntity<UserDto> getByEmail(@RequestParam String email) {
        User user = service.getUserByEmail(email);
        UserDto dto = mapper.mapUserToDto(user);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    /*
     * Handle request by url /user/bookSlot
     * Methode book slot in parking of given community
     *
     * @RequestParam String email
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * @RequestParam int slotNumber
     * */
    @PostMapping(value = "/bookSlot")
    @Transactional
    public ResponseEntity<List<Slot>> bookSlot(@RequestParam String email,
                                               @RequestParam String address,
                                               @RequestParam int parkingNumber,
                                               @RequestParam int slotNumber) {
        Slot slot = service.bookSlotByAddressAndNumber(email, address, parkingNumber, slotNumber);
        return new ResponseEntity(slot, HttpStatus.OK);
    }

    /*
     * Handle request by url /user/takeSlot
     * Methode take slot in parking of given community
     *
     * @RequestParam String email
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * @RequestParam int slotNumber
     * */
    @PostMapping(value = "/takeSlot")
    @Transactional
    public ResponseEntity<Slot> takeSlot(@RequestParam String email,
                                         @RequestParam String address,
                                         @RequestParam int parkingNumber,
                                         @RequestParam int slotNumber) {
        Slot slot = service.takeSlot(email, address, parkingNumber, slotNumber);
        return new ResponseEntity(slot, HttpStatus.OK);
    }

    /*
     * Handle request by url /user/releaseSlot
     * Methode release slot from parking of given community
     *
     * @RequestParam String email
     * @RequestParam String address
     * @RequestParam int parkingNumber
     * @RequestParam int slotNumber
     * */
    @PostMapping(value = "/releaseSlot")
    @Transactional
    public ResponseEntity<Slot> releaseSlot(@RequestParam String email,
                                            @RequestParam String address,
                                            @RequestParam int parkingNumber,
                                            @RequestParam int slotNumber) {
        Slot slot = service.releaseSlot(email, address, parkingNumber, slotNumber);
        return new ResponseEntity(slot, HttpStatus.OK);
    }
}
